# Mis .dotfiles

Mi set / LAB de configuraciones para zsh, git, ssh, bash y otras utilidades.

## Sobre stow

Básicamente es un administrador de enlaces simbólicos.  
"GNU Stow is a symlink farm manager which takes distinct packages of software and/or data located in separate directories on the filesystem, and makes them appear to be installed in the same place".  
De esta manera puedo tener un único repositorio con todos mis archivos de configuración y aplicarlos rápidamente con un comando.

## Recursos

- [Unofficial guide to dotfiles on GitHub](https://dotfiles.github.io/)
- [A curated list of dotfiles resources](https://github.com/webpro/awesome-dotfiles)
- [A set of vim, zsh, git, and tmux configuration files](https://github.com/thoughtbot/dotfiles)
- [How I setup my laptop with Ansible](https://www.giacomodebidda.com/posts/how-i-setup-my-laptop-with-ansible/)
- [Forkeado de @azacchino](https://github.com/azacchino/.dotfiles)
- [jackdbd dotfiles](https://github.com/jackdbd/dotfiles)
- [diodonfrost ubuntu-laptop-playbook](https://github.com/diodonfrost/ubuntu-laptop-playbook)

## Requerimientos

```bash
# Stow: symbolic link manager [info](https://alexpearce.me/2016/02/managing-dotfiles-with-stow/)
# Takes distinct packages of software and/or data located in separate directories on the filesystem, and makes them appear to be installed in the same place.
$ sudo apt install -y git stow
```

## Implementación

```bash
# Clonar repositorio
$ git clone git@github.com:diegobollini/.misdotfiles.git
$ cd .misdotfiles
# Fix rápido: borrar los archivos a aplicar si es que ya existen
# Ejecutar comando agregando las carpetas / utilidades que correspondan
$ cd ssh
$ stow . --target ~
```

## Troubleshooting

We may get some warning messages like the following one.

```sh
    cd ~/Dotfiles
    stow -n git
    WARNING! stowing git would cause conflicts:
      * existing target is neither a link nor a directory: .gitconfig
    All operations aborted.
```

This means that the file `.gitconfig` exists before the symlinking. We need to
manually change its name so GNU Stow can create the symlink. My recommendation is
to rename it:

```sh
    mv ~/.gitconfig ~/.gitconfig.old
```
